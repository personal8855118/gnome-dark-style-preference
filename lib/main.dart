import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  ThemeMode? _themeMode = ThemeMode.system;

  void setThemeMode(ThemeMode? mode) {
    setState(() {
      _themeMode = mode;
    });
  }

  Widget buildRadioListTile({required String title, required ThemeMode value}) {
    return ListTile(
      title: Text(title),
      leading: Radio<ThemeMode?>(
        value: value,
        groupValue: _themeMode,
        onChanged: setThemeMode,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My App',
      theme: ThemeData.light(useMaterial3: true),
      darkTheme: ThemeData.dark(useMaterial3: true),
      themeMode: _themeMode,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Theme'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              buildRadioListTile(title: 'Default', value: ThemeMode.light),
              buildRadioListTile(title: 'Dark', value: ThemeMode.dark),
              buildRadioListTile(
                  title: 'System Style Preference', value: ThemeMode.system),
            ],
          ),
        ),
      ),
    );
  }
}
